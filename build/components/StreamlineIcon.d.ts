import { FunctionComponent } from 'react';
import './StreamlineIcon.css';
declare type iconSlug = string;
declare type iconWidth = number;
declare type iconHeight = number;
declare type iconOptions = {
    fill: string;
    stroke: string;
    'stroke-linecap': 'butt' | 'round' | 'square' | 'inherit';
    'stroke-linejoin': 'miter' | 'round' | 'bevel' | 'inherit';
    'stroke-width': number | string;
};
declare type iconRepresentation = string;
export declare type Icon = [
    iconSlug,
    iconWidth,
    iconHeight,
    iconOptions[],
    iconRepresentation[]
];
declare const StreamlineIcon: FunctionComponent<{
    icon: Icon;
    spin?: boolean;
    infinite?: boolean;
    easeInOut?: boolean;
    fast?: boolean;
    size?: number;
    fill?: string;
    stroke?: string;
    width?: number;
    height?: number;
    customClassName?: string;
}>;
export default StreamlineIcon;
